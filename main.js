class Block_Description {
    title 
    message
    icon_path
    image_path

    constructor(title, message, icon_path, image_path) {
        this.title = title
        this.message = message
        this.icon_path = icon_path
        this.image_path = image_path
    }

    check_on_empty(value) {return (value.length > 0) ? true : false}

    set setTitle(value) {this.title = this.check_on_empty(value) ? value : console.log('empty value')}

    set setMessage(value) {this.message = this.check_on_empty(value) ? value : console.log('empty value')}

    set setIconPath(value) {this.icon_path = this.check_on_empty(value) ? value : console.log('empty value')}

    set setImagePath(value) {this.image_path = this.check_on_empty(value) ? value : console.log('empty value')}

    get getTitle() {return this.title}
    get getMessage() {return this.message}
    get getIconPath() {return this.icon_path}
    get getImagePath() {return this.image_path}
}

class Benefits_description extends Block_Description {
    benefits_list = []
    constructor(title, message, icon_path, image_path){
        super(title, message, icon_path, image_path)
    }

    set addBenefit(value) {this.benefits_list.push(this.check_on_empty(value) ? value : null)}
    get getBenefits() {return this.benefits_list}
}

class InfoRibbon {
    info_list = []
    constructor(title, message, icon_path, image_path) {
        let item = new Block_Description(title, message, icon_path, image_path)
        this.info_list.push(item)
    }

    get getInfoList() {return this.info_list}
    set addBlock(obj) {this.info_list.push(obj)}
}

let block1 = new Block_Description("title1", "message2", "iconkadfvvdf", "izobrazhenie21/2/1")
let f = new InfoRibbon("title", "message", "iconka", "izobrazhenie")

console.log(JSON.stringify(f))
f.addBlock = block1
console.log(JSON.stringify(f))
